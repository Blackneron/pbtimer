# PBtimer - README #
---

### Overview ###

The **PBtimer** tool is an application timer which can be used to start applications after a specified delay of time.

### Screenshots ###

![PBtimer - Debug output](development/readme/pbtimer1.png "PBtimer - Debug output")

![PBtimer - Customized shortcut](development/readme/pbtimer2.png "PBtimer - Customized shortcut")

### Setup ###

* Make a shortcut to the **PBtimer.exe** executable.
* Add the necessary parameters to the shortcut **Target**.
* Enable the **Debug Mode** (last parameter set to 1) for testing.
* Then you can place the shortcut into the **Autostart** directory.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBtimer** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
