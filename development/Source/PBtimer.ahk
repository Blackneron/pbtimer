; ======================================================================
;
;                      PBtimer - Application Timer
;
;                              Version 1.0
;
;              Copyright � 2015 / PB-Soft / Patrick Biegel
;
;                            www.pb-soft.com
;
; ======================================================================


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   S E T T I N G S
; ======================================================================

; Specify that more than one instance of this application can run.
#SingleInstance off

; Hide the tray icon.
#NoTrayIcon

; Enable autotrim.
AutoTrim, On

; Set the working directory.
SetWorkingDir, %A_ScriptDir%


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   I N F O R M A T I O N
; ======================================================================

; Specify the application name.
ApplName = PBtimer

; Specify the application version.
ApplVersion = 1.0

; Specify the internal application version.
ApplIntVersion = 32

; Calculate the application modification time.
FileGetTime, ModificationTime, PBtimer.exe

; Format the application build date.
FormatTime, ApplBuildDate , %ModificationTime%, dd.MM.yyyy / HH:mm:ss

; Specify the copyright text.
Copyright := "Copyright " . Chr(169) . " " . A_YYYY . " - PB-Soft"


; ======================================================================
; C H E C K   T H E   D E B U G   M O D E
; ======================================================================

; Specify the debug mode.
DebugMode = %6%

; Check if the debug information should be displayed.
If DebugMode = 1
{

  ; Display a debug message.
  MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Debug mode: Enabled, 5
}

; The debug information should not be displayed.
else
{

  ; Disable the debug mode.
  DebugMode = 0
}


; ======================================================================
; C H E C K   S T A R T   S L E E P   T I M E   P A R A M E T E R
; ======================================================================

; Specify the start sleep time in seconds.
StartSleepTimeSec = %5%

; Check if the start sleep time was passed.
If StartSleepTimeSec !=
{

  ; Specify the start sleep time.
  StartSleepTime := StartSleepTimeSec * 1000

  ; Check if the debug information should be displayed.
  If DebugMode = 1
  {

    ; Display a debug message.
    MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Start sleep time: %StartSleepTimeSec% seconds, 5
  }

  ; Wait the start sleep time.
  Sleep, %StartSleepTime%

} ; Check if the start sleep time was passed.

; No start sleep time was passed.
else
{

  ; Check if the debug information should be displayed.
  If DebugMode = 1
  {

    ; Display a debug message.
    MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, No start sleep time was specified., 5
  }

} ; No start sleep time was passed.


; ======================================================================
; C H E C K   T H E   A P P L I C A T I O N   P A T H
; ======================================================================

; Specify the application path.
ApplPath = %1%

; Check if an application path was passed.
If ApplPath !=
{

  ; Check if the debug information should be displayed.
  If DebugMode = 1
  {

    ; Display a debug message.
    MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Application path: %ApplPath%, 5
  }


  ; ====================================================================
  ; C H E C K   T H E   A P P L I C A T I O N   P A R A M E T E R S
  ; ====================================================================

  ; Specify the application parameters.
  ApplParameters = %2%

  ; Check if the application parameters were passed.
  If (ApplParameters != "" && ApplParameters != "none")
  {

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Application parameters: %ApplParameters%, 5
    }

  } ; Check if the application parameters were passed.

  ; No application parameters were passed.
  else
  {

    ; Specify the application parameters.
    ApplParameters = none

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, No application parameters were specified., 5
    }

  } ; No application parameters were passed.


  ; ====================================================================
  ; C H E C K   T H E   L O O P   M A X I M U M
  ; ====================================================================

  ; Specify the loop maximum.
  LoopMaximum = %3%

  ; Check if the loop maximum was passed.
  If LoopMaximum !=
  {

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Loop maximum: %LoopMaximum% trials, 5
    }

  } ; Check if the loop maximum was passed.

  ; No loop maximum was passed.
  else
  {

    ; Specify the loop maximum default.
    LoopMaximum = 50

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, No loop maximum was specified.`n`nUsing the default value of: %LoopMaximum% trials, 5
    }

  } ; No loop maximum was passed.


  ; ====================================================================
  ; C H E C K   T H E   L O O P   S L E E P   T I M E
  ; ====================================================================

  ; Specify the loop sleep time in seconds.
  LoopSleepTimeSec = %4%

  ; Check if the loop sleep time was passed.
  If LoopSleepTimeSec !=
  {

    ; Specify the loop sleep time.
    LoopSleepTime := LoopSleepTimeSec * 1000

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Loop sleep time: %LoopSleepTimeSec% seconds, 5
    }

  } ; Check if the loop sleep time was passed.

  ; No loop sleep time was passed.
  else
  {

    ; Specify the loop sleep time in seconds.
    LoopSleepTimeSec = 10

    ; Specify the loop sleep time default.
    LoopSleepTime := LoopSleepTimeSec * 1000

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, No loop sleep time was specified.`n`nUsing the default value of: %LoopSleepTimeSec% seconds, 5
    }

  } ; No loop sleep time was passed.


  ; ====================================================================
  ; S T A R T   T H E   A P P L I C A T I O N
  ; ====================================================================

  ; Loop and try to start the application.
  Loop, %LoopMaximum%
  {

    ; Check if the debug information should be displayed.
    If DebugMode = 1
    {

      ; Display a debug message.
      MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, Try to start the application ...`n`nLoop number: %A_Index%, 2
    }

    ; Check if the application file exist.
    IfExist, %ApplPath%
    {

      ; Check if the application parameters are not used.
      If ApplParameters = none
      {

        ; Start the application.
        Run, "%ApplPath%"
      }

      ; The application parameters are used.
      else
      {

        ; Start the application.
        Run, "%ApplPath%" "%ApplParameters%"
      }

      ; Check if the debug information should be displayed.
      If DebugMode = 1
      {

        ; Display a debug message.
        MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, The application was started !, 5
      }

      ; Break the loop.
      Break

    } ; Check if the application file exist.

    ; The file does not exist.
    else
    {

      ; Check if the debug information should be displayed.
      If DebugMode = 1
      {

        ; Display a debug message.
        MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, The application does not exist !`n`nWait %LoopSleepTimeSec% seconds ..., 2
      }

      ; Sleep some time.
      Sleep, %LoopSleepTime%

    } ; The file does not exist.

  } ; Loop and try to start the application.

} ; Check if an application path was passed.


; ======================================================================
; D I S P L A Y   T H E   H E L P   I N F O R M A T I O N
; ======================================================================

; No application path was specified - Display the help information.
else
{

  ; Check if the debug information should be displayed.
  If DebugMode = 1
  {

    ; Display a debug message.
    MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, No application path was specified !, 5
  }

  ; Create the help text.

  HelpText = %HelpText%Application Version: %ApplVersion%`n
  HelpText = %HelpText%Application Internal Version: %ApplIntVersion%`n
  HelpText = %HelpText%Application Build Date: %ApplBuildDate%`n
  HelpText = %HelpText%Application Copyright: %Copyright%`n`n
  HelpText = %HelpText%Please use PBtimer the following way:`n`n
  HelpText = %HelpText%PBtimer.exe Appl [Params [Loops [LTime [STime [Debug]]]]]`n`n
  HelpText = %HelpText%Please check the file 'Help.txt' for more information!

  ; Display a help message.
  MsgBox, 4160, %ApplName% %ApplVersion% - Help, %HelpText%

} ; No application path was specified - Display the help information.


; ======================================================================
; C H E C K   T H E   D E B U G   M O D E
; ======================================================================

; Check if the debug information should be displayed.
If DebugMode = 1
{

  ; Create the debug text.
  DebugText = Debug Summary:`n`n
  DebugText = %DebugText%Application Version: %ApplVersion%`n
  DebugText = %DebugText%Internal Version: %ApplIntVersion%`n
  DebugText = %DebugText%Application Build Date: %ApplBuildDate%`n
  DebugText = %DebugText%Application Copyright: %Copyright%`n`n
  DebugText = %DebugText%Working Directory: %A_WorkingDir%`n`n
  DebugText = %DebugText%Number of parameters: %0%`n`n
  DebugText = %DebugText%Parameter 1 - Appl: %ApplPath%`n
  DebugText = %DebugText%Parameter 2 - Params: %ApplParameters%`n
  DebugText = %DebugText%Parameter 3 - Loops: %LoopMaximum%`n
  DebugText = %DebugText%Parameter 4 - LTime: %LoopSleepTimeSec%`n
  DebugText = %DebugText%Parameter 5 - STime: %StartSleepTimeSec%`n
  DebugText = %DebugText%Parameter 6 - Debug: %DebugMode%`n`n

  ; Display a debug message.
  MsgBox, 4160, %ApplName% %ApplVersion% - Debug Information, %DebugText%
}

; Exit the application.
ExitApp


; ======================================================================
; E X I T   T H E   C H E C K N E T   A P P L I C A T I O N
; ======================================================================

; Specify the hotkey to exit the application - CTRL + ALT + X
^!x::

  ; Display an exit question.
  MsgBox, 4132, %ApplName% %ApplVersion%, Do you really want to exit ?

  ; Check if the yes button was pressed.
  IfMsgBox Yes
  {

    ; Exit the application.
    ExitApp

  } ; Check if the yes button was pressed.

return
